#ifndef WIFI_H
#define WIFI_H

#ifdef __cplusplus
extern "C"{
#endif 


void wifi_init();
bool wifi_is_connected();
void wifi_loop();


#ifdef __cplusplus
}
#endif

#endif //WIFI_H
