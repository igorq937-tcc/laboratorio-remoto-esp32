#ifndef NVS_UTIL_H
#define NVS_UTIL_H

#ifdef __cplusplus
extern "C"{
#endif 


typedef struct {
    char tb_token[25];
} nvs_data_s;

void nvs_util_setup(void);
nvs_data_s nvs_util_get_data(void);
void nvs_util_set_data(nvs_data_s *new_nvs_data);
void nvs_util_clear_data(void);


#ifdef __cplusplus
}
#endif

#endif //NVS_UTIL_H
