#ifndef THINGSBOARD_H
#define THINGSBOARD_H

#include <Espressif_MQTT_Client.h>
#include <ThingsBoard.h>
#include <Espressif_Updater.h>
#include <Shared_Attribute_Callback.h>

class ThingsBoardManager {
public:
    ThingsBoardManager();
    bool loop();
    bool connected();
    
    template<typename T>
    bool sendAttribute(const char *key, const T &value) {
        return tb.sendAttributeData(key, value);
    }

    template <size_t MaxAttributes = Default_Attributes_Amount>
    bool subscribe2SharedAttributes(Shared_Attribute_Callback<MaxAttributes> sharedAttributeCallback) {
        return tb.Shared_Attributes_Subscribe(sharedAttributeCallback);
    }

private:
    bool connect();
    void initializeNvs();

    Espressif_MQTT_Client mqttClient;
    ThingsBoard tb;
    Espressif_Updater updater;

    bool currentFWSent;
    bool updateRequestSent;
    bool initializedNvs;
    bool subscribed;
};

#endif //THINGSBOARD_H
