#include <esp_log.h>
#include <string.h>

#include "nvs_util.h"
#include "thingsboard_defs.h"
#include "ThingsBoardManager.h"

static const char* TAG = "TB_MANAGER";

static const uint16_t MAX_MESSAGE_SIZE = DEFAULT_FIRMWARE_PACKET_SIZE + 1024U;
static const char FIRMWARE_STATE_UPDATED[] = "UPDATED";

static void updatedCallbackStatic(const bool& success) {
    if (success) {
        ESP_LOGI(TAG, "Firmware update successful. Rebooting now...");
        esp_restart();
    } else {
        ESP_LOGI(TAG, "Firmware update failed.");
    }
}

static void progressCallbackStatic(const size_t& currentChunk, const size_t& totalChunks) {
    ESP_LOGI(TAG, "Firmware download progress: %.2f%%", static_cast<float>(currentChunk * 100U) / totalChunks);
}

ThingsBoardManager::ThingsBoardManager()
    : mqttClient(), tb(mqttClient, MAX_MESSAGE_SIZE), updater(),
      currentFWSent(false), updateRequestSent(false), initializedNvs(false), subscribed(false) {
    ESP_LOGI(TAG, "ThingsBoardManager instance created");
}

bool ThingsBoardManager::connect() {
    if (!tb.connected()) {
        ESP_LOGI(TAG, "Connecting to ThingsBoard server...");
        if (tb.connect(DEFAULT_SERVER, nvs_util_get_data().tb_token, DEFAULT_SERVER_PORT)) {
            ESP_LOGI(TAG, "Connected to ThingsBoard successfully.");
            return true;
        } else {
            ESP_LOGI(TAG, "Failed to connect to ThingsBoard, retrying...");
            return false;
        }
    }

    return true;
}

void ThingsBoardManager::initializeNvs() {
    ESP_LOGI(TAG, "Initializing NVS data for ThingsBoard...");
    nvs_data_s nvs_data = nvs_util_get_data();

    if (nvs_data.tb_token[0] == '\0') {
        ESP_LOGI(TAG, "No token found in NVS, using default token.");
        strcpy(nvs_data.tb_token, DEFAULT_TOKEN);
        nvs_util_set_data(&nvs_data);
    }
}

bool ThingsBoardManager::loop() {
    if (!initializedNvs) {
        initializeNvs();
        initializedNvs = true;
    }

    if (!connect()) {
        ESP_LOGI(TAG, "Failed to connect to ThingsBoard.");
        return false;
    }

    if (!currentFWSent) {
        ESP_LOGI(TAG, "Sending firmware info...");
        currentFWSent = tb.Firmware_Send_Info(DEFAULT_FIRMWARE_TITLE, DEFAULT_FIRMWARE_VERSION) &&
                        tb.Firmware_Send_State(FIRMWARE_STATE_UPDATED);

        if (currentFWSent) {
            ESP_LOGI(TAG, "Firmware info sent successfully.");
        } else {
            ESP_LOGI(TAG, "Failed to send firmware info.");
        }
    }

    if (!updateRequestSent) {
        ESP_LOGI(TAG, "Subscribing to firmware update...");
        OTA_Update_Callback callback(&progressCallbackStatic,
                                     &updatedCallbackStatic,
                                     DEFAULT_FIRMWARE_TITLE, DEFAULT_FIRMWARE_VERSION,
                                     &updater, DEFAULT_FIRMWARE_FAILURE_RETRIES,
                                     DEFAULT_FIRMWARE_PACKET_SIZE);
        updateRequestSent = tb.Subscribe_Firmware_Update(callback);

        if (updateRequestSent) {
            ESP_LOGI(TAG, "Subscribed to firmware updates successfully.");
        } else {
            ESP_LOGI(TAG, "Failed to subscribe to firmware updates.");
        }
    }

    return tb.loop();
}

bool ThingsBoardManager::connected() {
    return tb.connected();
}
