#include <string.h>
#include <assert.h>

#include "esp_log.h"
#include "esp_wifi.h"
#include "esp_netif.h"

#include "wifi.h"
#include "wifi_defs.h"


bool is_connected = false;

static void wifi_on_got_ip(void* event_handler_arg, esp_event_base_t event_base, int32_t event_id, void* event_data) {
    is_connected = true;
    ESP_LOGI(__func__, "WiFi connected");
}

static void wifi_on_disconnect(void* event_handler_arg, esp_event_base_t event_base, int32_t event_id, void* event_data) {
    is_connected = false;
    ESP_LOGI(__func__, "WiFi disconnected");
}

void wifi_init() {
    wifi_init_config_t wifi_init_config = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&wifi_init_config));

    esp_netif_config_t netif_config = ESP_NETIF_DEFAULT_WIFI_STA();
    esp_netif_t *netif = esp_netif_new(&netif_config);
    assert(netif);

    ESP_ERROR_CHECK(esp_netif_attach_wifi_station(netif));
    ESP_ERROR_CHECK(esp_event_handler_register(IP_EVENT, IP_EVENT_STA_GOT_IP, &wifi_on_got_ip, NULL));
    ESP_ERROR_CHECK(esp_event_handler_register(WIFI_EVENT, WIFI_EVENT_STA_DISCONNECTED, &wifi_on_disconnect, NULL));
    ESP_ERROR_CHECK(esp_wifi_set_default_wifi_sta_handlers());
    ESP_ERROR_CHECK(esp_wifi_set_storage(WIFI_STORAGE_RAM));

    wifi_config_t wifi_config;
    memset(&wifi_config, 0, sizeof(wifi_config));
    strncpy((char*)wifi_config.sta.ssid, DEFAULT_SSID, strlen(DEFAULT_SSID) + 1);
    strncpy((char*)wifi_config.sta.password, DEFAULT_PASSWORD, strlen(DEFAULT_PASSWORD) + 1);

    ESP_LOGI(__func__, "Connecting to %s WiFi...", wifi_config.sta.ssid);
    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
    ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_STA, &wifi_config));
    ESP_ERROR_CHECK(esp_wifi_start());
    ESP_ERROR_CHECK(esp_wifi_connect());
}

bool wifi_is_connected() {
    return is_connected;
}

void wifi_loop() {
    if (!is_connected) {
        ESP_LOGI(__func__, "WiFi not connected, retrying...");
        ESP_ERROR_CHECK(esp_wifi_connect());
    }
}
