#include <esp_log.h>
#include <nvs_flash.h>
#include <esp_netif.h>
#include <esp_event.h>
#include <esp_random.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"

#include "wifi.h"
#include "ThingsBoardManager.h"
#include "nvs_util.h"

ThingsBoardManager tbManager;

void wifi_task(void *pvParameters) {
    wifi_init();

    while (true) {
        vTaskDelay(10000 / portTICK_PERIOD_MS);
        wifi_loop();
    }
}

void thingsboard_task(void *pvParameters) {
    while (true) {
        vTaskDelay(10000 / portTICK_PERIOD_MS);
        if (!wifi_is_connected()) continue;
        tbManager.loop();
    }
}

void processSharedAttributeUpdate(const JsonObjectConst data) {
    for (auto it = data.begin(); it != data.end(); ++it) {
        if (!strcmp(it->key().c_str(), "LED_BUILTIN")) {
            gpio_set_level(GPIO_NUM_2, it->value().as<bool>());
        }
    }
}

void attribute_telemetry_task(void *pvParameters) {
    gpio_reset_pin(GPIO_NUM_2);
    gpio_set_direction(GPIO_NUM_2, GPIO_MODE_OUTPUT);
    const std::array attribute_key = { "LED_BUILTIN" };
    const Shared_Attribute_Callback attribute_key_change_callback(&processSharedAttributeUpdate, attribute_key.cbegin(), attribute_key.cend());
    bool subscribed = false;

    while (true) {
        vTaskDelay(10000 / portTICK_PERIOD_MS);
        if (!tbManager.connected()) continue;
        tbManager.sendAttribute("random_value", esp_random());

        if (!subscribed) {
            tbManager.subscribe2SharedAttributes(attribute_key_change_callback);
            subscribed = true;
        }
    }

}

extern "C" void app_main() {
    ESP_LOGI(__func__, "Startup...");
    ESP_LOGI(__func__, "Free memory: %" PRIu32 " bytes", esp_get_free_heap_size());
    ESP_LOGI(__func__, "IDF version: %s", esp_get_idf_version());

    ESP_ERROR_CHECK(nvs_flash_init());
    ESP_ERROR_CHECK(esp_netif_init());
    ESP_ERROR_CHECK(esp_event_loop_create_default());
    nvs_util_setup();

    xTaskCreate(&wifi_task, "wifi_task", 4096, NULL, 5, NULL);
    xTaskCreate(&thingsboard_task, "thingsboard_task", 4096, NULL, 5, NULL);
    xTaskCreate(&attribute_telemetry_task, "attribute_telemetry_task", 8192, NULL, 5, NULL);
}
