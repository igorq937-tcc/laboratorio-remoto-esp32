#include <nvs_component.h>
#include <stdlib.h>
#include <string.h>

#include "nvs_util.h"

static nvs_data_s *nvs_data = NULL;

nvs_data_s nvs_util_get_data(void){
    return *nvs_data;
}

void nvs_util_reset_data(void){
    nvs_data->tb_token[0] = '\0';
}

void nvs_util_setup(void){
    nvs_data = malloc(sizeof(nvs_data_s));
    nvs_util_reset_data();
    read_struct("nvs_data", (void**)&nvs_data, sizeof(nvs_data_s));
}

void nvs_util_set_data(nvs_data_s *new_nvs_data){
    ESP_ERROR_CHECK(write_struct("nvs_data", new_nvs_data, sizeof(nvs_data_s)));
    memcpy(nvs_data, new_nvs_data, sizeof(nvs_data_s));
}

void nvs_util_clear_data(void){
    nvs_util_reset_data();
    ESP_ERROR_CHECK(write_struct("nvs_data", nvs_data, sizeof(nvs_data_s)));
}
